package com.company.entities;

import com.company.enums.Drift;
import com.company.interfaces.Entity;

public class Asterisk implements Entity {
    private int prevLine;
    private int prevColumn;
    private int nowLine;
    private int nowColumn;
    public int getPrevLine() {
        return prevLine;
    }

    public int getPrevColumn() {
        return prevColumn;
    }

    public int getNowLine() {
        return nowLine;
    }

    public int getNowColumn() {
        return nowColumn;
    }

    public Asterisk(int line, int column) {
        this.nowLine = line;
        this.nowColumn = column;
    }

    public void setNowLine(int nowLine) {
        this.nowLine = nowLine;
    }

    public void setNowColumn(int nowColumn) {
        this.nowColumn = nowColumn;
    }

    /**
     * Перемещение игрока к другой ячейке поля.
     * @param drift Направление движения.
     */
    @Override
    public void moving(String drift) {
        Drift route = Drift.valueOf(drift);
        switch (route) {
            case LEFT:
                prevLine = nowLine;
                prevColumn = nowColumn;
                nowColumn = nowColumn - 1;
                break;
            case RIGHT:
                prevLine = nowLine;
                prevColumn = nowColumn;
                nowColumn = nowColumn + 1;
                break;
            case UP:
                prevColumn = nowColumn;
                prevLine = nowLine;
                nowLine = nowLine - 1;
                break;
            case DOWN:
                prevColumn = nowColumn;
                prevLine = nowLine;
                nowLine = nowLine + 1;
                break;
            case DDR:
                prevLine = nowLine;
                nowLine = nowLine +1;
                prevColumn = nowColumn;
                nowColumn = nowColumn + 1;
                break;
            case DDL:
                prevLine = nowLine;
                nowLine = nowLine + 1;
                prevColumn = nowColumn;
                nowColumn = nowColumn - 1;
                break;
        }
    }
}
