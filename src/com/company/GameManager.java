package com.company;

import com.company.entities.Asterisk;
import com.company.entities.Cross;
import com.company.enums.Drift;
import com.company.field.Field;
import com.company.field.Mesh;
import com.company.interfaces.Entity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class GameManager {
	static final int HEIGHT = 5;
	static final int WIDTH = 3;
    static final char markA = '*';
    static final char markX = 'X';
    private  static Mesh beginChordCOne;
    private  static Mesh beginChordCTwo;
    private  static Mesh beginChordCThree;
    private  static Mesh beginChordA;
    private  static Field field;
    private  static Entity playerCOne;
    private  static Entity playerCTwo;
    private  static Entity playerCThree;
    private  static Entity playerA;
    private static int id;
    private static boolean flag;
	public static void main(String[] arg) throws IOException {
		beginChordCOne = new Mesh(3,0);
		beginChordCTwo = new Mesh(4,1);
		beginChordCThree = new Mesh(3,2);
		beginChordA = new Mesh(2,1);
	    field = new Field(HEIGHT, WIDTH);
		field.create();
		field.set(beginChordA.getLine(), beginChordA.getColumn(), markA);
		field.set(beginChordCOne.getLine(), beginChordCOne.getColumn(), markX);
		field.set(beginChordCTwo.getLine(), beginChordCTwo.getColumn(), markX);
		field.set(beginChordCThree.getLine(), beginChordCThree.getColumn(), markX);
		playerCOne = new Cross(beginChordCOne.getLine(), beginChordCOne.getColumn());
	    playerCTwo = new Cross(beginChordCTwo.getLine(), beginChordCTwo.getColumn());
		playerCThree = new Cross(beginChordCThree.getLine(), beginChordCThree.getColumn());
		playerA = new Asterisk(beginChordA.getLine(), beginChordA.getColumn());
		field.drawField();
		System.out.print(readingFromFile("rules/rule.txt"));
		int n = 1;
		id = 1;
		flag = true;
		while (flag) {
			Scanner in = new Scanner(System.in);
			if (id == 1) {
				System.out.println("\nВыберите какой из 3 фигур будете ходить, Введите 1, 2 или 3");
				String number = in.nextLine();
				System.out.println("\n" + n +' ' + "ХОД.  ПЕРВЫЙ ИГРОК, введите одно из перечисленных значений UP, DUR(DIAGUPRIGHT), DUL(DIAGUPLEFT).");
				in = new Scanner(System.in);
				String command = in.nextLine().toUpperCase();
						switch (number) {
							case "1":
							    if( isPossible(command, (Cross) playerCOne))
								validator(command,playerCOne,markX);
								break;
							case "2":
                                if( isPossible(command, (Cross) playerCTwo))
                                validator(command,playerCTwo,markX);
								break;
							case "3":
                                if( isPossible(command, (Cross) playerCThree))
                                validator(command,playerCThree,markX);
								break;
							default:
								System.out.println("Нет такого номера игрока, введите заново");
						}
					}
			 else {
				System.out.println("\n" + n +' ' + "ХОД. ВТОРОЙ ИГРОК. Введите одно из перечисленных значений  - LEFT, RIGHT, UP, DOWN ,DDR(DIAGDOWNRIGHT), DDL(DIAGDOWNLEFT).");
				in = new Scanner(System.in);
				String command = in.nextLine().toUpperCase();
					if (!command.equals(String.valueOf(Drift.DDL)) &&
							!command.equals(String.valueOf(Drift.DDR)) &&
							!command.equals(String.valueOf(Drift.UP)) &&
							!command.equals(String.valueOf(Drift.LEFT)) &&
							!command.equals(String.valueOf(Drift.RIGHT)) &&
							!command.equals(String.valueOf(Drift.DOWN))) {
						System.out.println("Такого хода для этого игрока не существует, введите правильно команду");
					} else if ((command.equals(String.valueOf(Drift.DDR)) || command.equals(String.valueOf(Drift.DDL))) && !(playerA.getNowLine() == 0 && playerA.getNowColumn() == 1)) {
						System.out.println("В этой клетке нельзя так ходить, повторите ход");
					} else {
						validator(command, playerA, markA);
					}
			}
			n++;
		}
	}

	/**
	 * Чтение правил из файла
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	private static String readingFromFile(String filePath) throws IOException {
		return new String(Files.readAllBytes(Paths.get(filePath)));
	}

	/**
	 *
	 * @param command Команда направления движения
	 * @param player Сущность
	 * @param mark Символ для сущности
	 */
	private static void validator(String command, Entity player, char mark) {
            player.moving(command);
            if(!field.exist(player.getNowLine(),player.getNowColumn())){
                System.out.println("Вы вышли за границу поля, повторите ход");
                player.setNowColumn(player.getPrevColumn());
                player.setNowLine(player.getPrevLine());
            } else if(field.getArrayMesh(player.getNowLine(),player.getNowColumn()) != ' '){
                System.out.println("Эта клетка уже занята, повторите ход");
                player.setNowColumn(player.getPrevColumn());
                player.setNowLine(player.getPrevLine());
            } else if((playerA.getNowLine() > playerCOne.getNowLine()) && (playerA.getNowLine() > playerCTwo.getNowLine()) && (playerA.getNowLine() > playerCThree.getNowLine())){
                field.set(player.getPrevLine(), player.getPrevColumn(), ' ');
                field.set(player.getNowLine(), player.getNowColumn(), mark);
                field.drawField();
                System.out.println("Победа второго игрока!");
                flag = false;
            } else{
                field.set(player.getPrevLine(), player.getPrevColumn(), ' ');
                field.set(player.getNowLine(), player.getNowColumn(), mark);
                field.drawField();
				if(id == 1) {
					id = 2;
				} else{
					id = 1;
				}
				if((isCorrect(playerA.getNowLine(),playerA.getNowColumn()+1) && isCorrect(playerA.getNowLine()-1,playerA.getNowColumn())
							&& isCorrect(playerA.getNowLine(),playerA.getNowColumn()-1) && isCorrect(playerA.getNowLine()+1,playerA.getNowColumn()))
							|| (playerA.getNowLine() == 0 && playerA.getNowColumn() == 1 && (isCorrect(1,0) && isCorrect(1,2)))){
						flag = false;
						System.out.println("Победа первого игрока игрока!, У второго игрока нет ходов");
					}
				}
        }

	/**
	 * Проверка на существование ясейки и на ее пустоту
	 * @param line Строка
	 * @param column Столбец
	 * @return true ячейка не существует/ячейка не "пустая" (под пустотой ячейки подразумевается нахождение в ней сивола ' ')
	 * false ячейка существует/ячейка "пустая"
	 */
	private  static boolean isCorrect(int line, int column){
    if(!field.exist(line,column))
    	return true;
    else if(field.getArrayMesh(line,column)!= ' ')
    	return  true;
    return  false;
	}

	/**
	 * проверка возможности введеного хода для первого игрока/ проверка может ли первый игрок ходить из определенных клеток по диагонали
	 * @param command Направление
	 * @param player Сущность
	 * @return false игрок не может ходить в эту клетку
	 */
    private static boolean isPossible(String command, Cross player){
        if ((command.equals(String.valueOf(Drift.DUR)) || command.equals(String.valueOf(Drift.DUL))) && !((player.getNowLine() == 1 && player.getNowColumn() == 0) || (player.getNowLine() == 1 && player.getNowColumn() == WIDTH -1))) {
            System.out.println("В этой клетке нельзя так ходить, повторите ход");
            return false;
        } else if (!(command.equals(String.valueOf(Drift.UP)) || command.equals(String.valueOf(Drift.DUL)) || command.equals(String.valueOf(Drift.DUR)))){
            System.out.println("Такого хода для этого игрока не существует, введите правильно команду");
            return false;
        }
        return  true;
    }
}
