package com.company.interfaces;

public interface Entity {
    public void setNowLine(int nowLine);
    public void setNowColumn(int nowColumn);
    public int getPrevLine();
    public int getPrevColumn();
    public int getNowLine();
    public int getNowColumn();

    /**
     * Перемещение игрока к другой ячейке поля.
     * @param drift Направление движения.
     */
    public void moving(String drift);
}
