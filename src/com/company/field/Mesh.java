package com.company.field;

public class Mesh {
    private int line;
    private int column ;
    private char value = ' ';

    public int getLine() {
        return line;
    }

    public int getColumn() {
        return column;
    }

    public char getValue() {
        return value;
    }

    public void setValue(char value) {
        this.value = value;
    }

    public Mesh(int line, int column) {
        this.line = line;
        this.column = column;
    }
}
