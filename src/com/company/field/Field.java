package com.company.field;

public  class Field {
    private int height;
    private int width;
    private Mesh[][] arrayMesh;

    /**
     * Получение символа из ячейки поля
     * @param line Строка
     * @param column Столбец
     * @return Символ в ячейке
     */
    public char getArrayMesh(int line, int column) {
        return arrayMesh[line][column].getValue();
    }

    public Field(int height, int width) {
        this.height = height;
        this.width = width;
    }

    /**
     * Создание игрового поля
     * @return Двумерный массив из ячеек(mesh)
     */
    public  Mesh[][] create(){
        arrayMesh = new Mesh[height][width];
        for (int i = 0 ; i < height; i++){
            for(int j = 0; j < width; j++){
                arrayMesh[i][j] = new Mesh(i,j);
            }
        }
        return arrayMesh;
    }

    /**
     * Отрисовка игрового поля
     */
    public void drawField(){
        for (int i = 0 ; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((arrayMesh[i][j].getLine() == 0 && arrayMesh[i][j].getColumn() == 0) ||
                        (arrayMesh[i][j].getLine() == 0 && arrayMesh[i][j].getColumn() == width-1) ||
                        (arrayMesh[i][j].getLine() == height-1 && arrayMesh[i][j].getColumn() == 0) ||
                        (arrayMesh[i][j].getLine() == height-1 && arrayMesh[i][j].getColumn() == width-1)){
                    System.out.print("   ");
                }
                else {
                    System.out.print("[" + arrayMesh[i][j].getValue() + "]");
                }
            }
            System.out.print('\n');
        }
    }

    /**
     * Установка передаваемого символа в ячейку
     * @param line Строка
     * @param column Столбец
     * @param s Символ
     */
    public void set(int line, int column, char s){
        arrayMesh[line][column].setValue(s);
}

    /**
     * Проверка существования ячейки в игровом поле
     * @param line Строка
     * @param column Столбец
     * @return true - существует, false не существует
     */
    public boolean exist(int line, int column){
        if(((line >= 0 && line < height) && (column >= 0 && column < width))
                && !((line == 0 && column == 0) ||(line == 0 && column == width-1)
                ||(line == height-1 && column == 0) ||(line == height-1 && column == width-1)))
           return true;
        return false;
    }
}
