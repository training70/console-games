package com.company.enums;

public enum Drift {
    LEFT,
    RIGHT,
    UP,
    DOWN ,
    DUR,
    DDR,
    DUL,
    DDL;
}

